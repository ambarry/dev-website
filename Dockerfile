FROM ubuntu:18.04 as builder
LABEL maintainer="alexanderbarry.dev@gmail.com"

RUN apt-get update

# install ca-certificates for Let's Encrypt
RUN apt-get install -y ca-certificates
RUN update-ca-certificates

# install git
RUN apt-get -y install git

# install go
RUN apt-get -y install software-properties-common
RUN add-apt-repository ppa:gophers/archive
RUN apt-get update
RUN apt-get -y install golang-1.11-go

# add to PATH
ENV PATH="/usr/lib/go-1.11/bin:${PATH}"

# cache app deps
WORKDIR /app
ADD go.mod .
ADD go.sum .
RUN go mod download

# build (any other packages, files, make this dynamic)
# TODO: need to handle other subdirs now
ADD *.go .
RUN go build

# -------------------------

# runtime container
FROM ubuntu:18.04

# TODO: install sqlite? mount volume?
RUN apt-get -y update
RUN apt-get -y upgrade
#RUN apt-get install -y sqlite3 libsqlite3-dev

WORKDIR /app
RUN mkdir /app/data

# get the ca-certificates
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# get the binary

COPY --from=builder /app/dev-website .

# add static content
ADD web web

# persist db
VOLUME ["/app/data"]

# allow cert caching
VOLUME ["/app/cert-cache"]

ENTRYPOINT ["./dev-website"]




