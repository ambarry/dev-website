package db

import (
	"database/sql"
)

// TODO: auth!


// Setup sets up the db
func Open(dbPath string) (*sql.DB, error) {
	db, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		return nil, err
	}
	return db, nil
}