package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/crypto/acme/autocert"
)

const (
	staticDir    = "./web/static"
	certCacheDir = "cert-cache"
	dbPath       = "data/dev-web.db"
)

var hosts = []string{
	"alexanderbarry.dev",
	"www.alexanderbarry.dev",
}

func main() {
	var local bool
	var port int
	flag.BoolVar(&local, "local", false, "run in local mode without let's encrypt")
	flag.IntVar(&port, "port", 80, "port of http server, defaults to 80")
	flag.Parse()
	log.Println("running local:", local)

	// main handler
	mux := http.NewServeMux()
	mux.Handle("/", http.FileServer(http.Dir(staticDir)))

	// TODO: setup blog db
	//blogDB := startDB()
	//mux.Handle("/blog", blog.Routes())

	addr := fmt.Sprintf(":%d", port)
	srv := http.Server{
		Addr:    addr,
		Handler: mux,
	}

	if !local {
		// Let's Encrypt cert manager
		mgr := autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: autocert.HostWhitelist(hosts...), // our domains
			Cache:      autocert.DirCache(certCacheDir),  // folder for storing certs
		}

		// TLS server for our mux, using Let's Encrypt to fetch cert
		srv = http.Server{
			Addr:      ":443",
			TLSConfig: mgr.TLSConfig(),
			Handler:   mux,
		}

		// start the https server
		//go serveHTTPS(&tlsServer)

		// add handler to regular http server to handle Let's Encrypt challenges.
		// the handler wrapper will take care of LE,
		// but pass all other urls to our handler

		//httpServer.Handler = mgr.HTTPHandler(nil)
	}

	// block forever
	serveHTTP(&srv, !local)
}

func serveHTTP(srv *http.Server, tls bool) {
	log.Printf("Starting server on %s with tls: %v\n", srv.Addr, tls)
	var err error
	if tls {
		err = srv.ListenAndServeTLS("", "")
	} else {
		err = srv.ListenAndServe()
	}
	log.Fatalln(err)
}
