#!/bin/bash

docker run \
  -it \
  --restart always \
  -p 80:80 \
  -p 443:443 \
  -v cert-cache:/app/cert-cache \
  -d \
  alexanderbarry/dev-website
